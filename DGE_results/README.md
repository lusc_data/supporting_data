# Differential gene expression results
This repository contain data accociated with publication:  
DGE for Subtype-1 Vs Normal and Subtype-2 Vs. Normal samples

===== Results for subtype_1_vs_Normal =====  
p-value cutoff : 0.05  
LogFC >= 2 (up) : 4586, 8.78%  
LogFC >= -2 (down) : 1495, 2.86%  

===== Results for subtype_2_vs_Normal =====  
p-value cutoff : 0.05  
LogFC >= 2 (up) : 5015, 9.60%  
LogFC >= -2 (down) : 3224, 6.17%  
